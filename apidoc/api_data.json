[
  {
    "type": "post",
    "url": "/rest/surveillance/add",
    "title": "Add new surveillance",
    "name": "AddSurveillance",
    "group": "Surveillance",
    "description": "<p>Add the surveillance item given in the request body.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Surveillance",
            "description": "<p>Object created.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "Error",
            "description": "<p>Return the error</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Surveillance"
  },
  {
    "type": "delete",
    "url": "/rest/surveillance/:surveillanceId/delete",
    "title": "Delete surveillance",
    "name": "DeleteSurveillance",
    "group": "Surveillance",
    "description": "<p>Delete the surveillance item.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Surveillances unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Message",
            "description": "<p>Deletion confirmation message with status.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Surveillance"
  },
  {
    "type": "put",
    "url": "/rest/surveillance/:surveillanceId/edit",
    "title": "Modify surveillance information",
    "name": "EditSurveillance",
    "group": "Surveillance",
    "description": "<p>Update the surveillance item with the data given in the request body.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Surveillances unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Surveillance",
            "description": "<p>Object modified.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "Error",
            "description": "<p>Return the error</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Surveillance"
  },
  {
    "type": "get",
    "url": "/rest/surveillance/:id",
    "title": "Request surveillance information",
    "name": "GetSurveillance",
    "group": "Surveillance",
    "description": "<p>Return a Object containing the surveillance information.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Surveillances unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "Surveillance",
            "description": "<p>Object.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "Error",
            "description": "<p>Return the error</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Surveillance"
  },
  {
    "type": "get",
    "url": "/rest/surveillances",
    "title": "List the surveillances",
    "name": "GetSurveillances",
    "group": "Surveillance",
    "description": "<p>List the surveillances sorted by date. You can use some optional parameter to filter those surveillance items.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "limit",
            "defaultValue": "10",
            "description": "<p>Optional limit with default 10.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category",
            "description": "<p>Optional category.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>Optional title.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://localhost/rest/surveillances?limit=2&category=CategoryName&title=Title"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "Array",
            "description": "<p>Array of the 10 last surveillances.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"_id\": \"58e3bd27c0850d02a4bd5f8e\",\n    \"time\": \"2017-04-04T15:35:03.544Z\",\n    \"category\": \"Name of the Category\",\n    \"title\": \"Title of the Surveillance\",\n    \"details\": \"Autem dolorem et officia qui voluptatum architecto. Sunt sapiente aliquid sit labore. Similique quasi sapiente ullam modi iure debitis accusamus et.\",\n    \"thumbnail\": \"/uploads/320x300-753d9524d4323baa0675df09111086ce\",\n    \"image\": \"/uploads/768x720-753d9524d4323baa0675df09111086ce\",\n    \"original\": \"public/uploads/753d9524d4323baa0675df09111086ce\",\n    \"__v\": 0,\n    \"coordinates\": {\n      \"lat\": 60.16985569999999,\n      \"lng\": 24.93837899999994\n    }\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "Error",
            "description": "<p>Return the error</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "groupTitle": "Surveillance"
  }
]
