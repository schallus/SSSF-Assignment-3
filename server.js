const express = require('express');
const mongoose = require('mongoose'); // MongoDB package
mongoose.Promise = global.Promise; // ES6 Promise
const multer = require('multer'); // Middleware for handling multipart/form-data
const sharp = require('sharp'); // Middleware for image optimisation
sharp.cache(false); // Disable the sharp cache to avoid some errors on Windows
const fs = require('fs');
const bodyParser = require('body-parser'); // Used by Passport
const session = require('express-session'); // Used by Passport to store the user session
const flash = require('connect-flash'); // Used to display error message on authentication error
const cookieParser = require('cookie-parser');

require('dotenv').config();

const app = express();
const passport = require('passport');

app.use(express.static('public'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
    secret: 'Q97q38eX1TuAd1gdFBbz7s82AheKqOX6OSkFQmtF',
    resave: true,
    saveUninitialized: false,
    cookie: {
        secure: false,
    },
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

const LocalStrategy = require('passport-local').Strategy;
passport.use(new LocalStrategy({
		usernameField: 'username',
		passwordField: 'password',
    },
    (username, password, done) => {
        if (username !== process.env.APP_USERNAME || password !== process.env.APP_PASSWORD) {
            return done(null, false, {message: 'Incorrect credentials.'});
        }
        return done(null, {username: username});
    }
));

passport.serializeUser((user, done) => {
    console.log('serializeUser');
    console.log(user);
    done(null, user);
});

passport.deserializeUser((user, done) => {
    console.log('deserializeUser');
    console.log(user);
    done(null, user);
});

const isAuthenticated = (req, res, next) => {
    // Check is the user is authenticated

    console.log('req.user: ' + req.user);

    next();

    /* if (req.user.authenticated)
        return next();

    // If the user is not connected, redirect him to the login page
    res.redirect('/login'); */
};

/* const BasicStrategy = require('passport-http').BasicStrategy;
passport.use(new BasicStrategy({
		usernameField: 'username',
		passwordField: 'password',
    },
    (username, password, done) => {
        console.log(username);
        if (username !== process.env.APP_USERNAME || password !== process.env.APP_PASSWORD) {
            return done(null, false);
        }
        return done(null, true);
    }
)); */

// Manage database connection
const DB = require('./model/database');
// Get model
const model = require('./model/model');

DB.connect().then(() => {
    console.log('Connected to MongoDB server');
    app.listen(process.env.APP_HTTP_PORT);
    console.log(`Listening on port ${process.env.APP_HTTP_PORT}`);
}, (err) => {
    console.log(err.message);
    console.error('Connection to MongoDB server failed');
});

/* 
// Import SSL Key and Certificate
const sslkey = fs.readFileSync('ssl-key.pem');
const sslcert = fs.readFileSync('ssl-cert.pem');

const options = {
      key: sslkey,
      cert: sslcert,
};

const https = require('https');
DB.connect().then(() => {
    console.log('Connected to MongoDB server');
    https.createServer(options, app).listen(process.env.APP_HTTPS_PORT);
    console.log(`Listening on port ${process.env.APP_HTTPS_PORT}`);
}, (err) => {
    console.log(err.message);
    console.error('Connection to MongoDB server failed');
}); */

/* app.set('view engine', 'pug');

app.get('/login', (req, res) => {
    res.render('login', {
        title: 'Login',
        error: req.flash('error')[0],
    });
});

app.post('/login',
    passport.authenticate('local', {
        failureRedirect: '/login',
        failureFlash: 'Invalid username or password.',
        session: true,
    }),
    (req, res) => {
        // If this function gets called, authentication was successful.
        console.log('Authentication success');
        console.log(req.user);
        res.redirect('/');
    }
); */

// Rest API Router
const apiRouter = express.Router();
app.use('/rest', apiRouter);

// Upload folder for multer
const upload = multer({
    dest: 'public/uploads/',
});

/**
 * @api {get} /rest/surveillances List the surveillances
 * @apiName GetSurveillances
 * @apiGroup Surveillance
 * @apiDescription List the surveillances sorted by date.
 * You can use some optional parameter to filter those surveillance items.
 *
 * @apiParam {Number} limit=10 Optional limit with default 10.
 * @apiParam {String} category Optional category.
 * @apiParam {String} title Optional title.
 *
 * @apiSampleRequest http://localhost/rest/surveillances?limit=2&category=CategoryName&title=Title
 *
 * @apiSuccess {Object[]} Array Array of the 10 last surveillances.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [
 *       {
 *         "_id": "58e3bd27c0850d02a4bd5f8e",
 *         "time": "2017-04-04T15:35:03.544Z",
 *         "category": "Name of the Category",
 *         "title": "Title of the Surveillance",
 *         "details": "Autem dolorem et officia qui voluptatum architecto. Sunt sapiente aliquid sit labore. Similique quasi sapiente ullam modi iure debitis accusamus et.",
 *         "thumbnail": "/uploads/320x300-753d9524d4323baa0675df09111086ce",
 *         "image": "/uploads/768x720-753d9524d4323baa0675df09111086ce",
 *         "original": "public/uploads/753d9524d4323baa0675df09111086ce",
 *         "__v": 0,
 *         "coordinates": {
 *           "lat": 60.16985569999999,
 *           "lng": 24.93837899999994
 *         }
 *       }
 *     ]
 *
 * @apiError {Object} Error Return the error
 */
apiRouter.get('/surveillances', isAuthenticated, (req, res) => {
    console.log('router /surveillances');

    console.log(req.user);

    const limit = (req.query.limit) ? parseInt(req.query.limit) : 10;
    const category = (req.query.category) ? req.query.category : null;
    const title = (req.query.title) ? req.query.title : null;

    const surveillances = model.Surveillance.find();
    surveillances.sort({time: -1});
    surveillances.limit(limit);
    if(category) surveillances.where('category').equals(category);
    if(title) surveillances.where('title').equals(title);
    surveillances.exec((err, surveillances) => {
        if(err) {
            console.log(err);
            res.json({error: err});
        }
        res.json(surveillances);
    });
});

/**
 * @api {get} /rest/surveillance/:id Request surveillance information
 * @apiName GetSurveillance
 * @apiGroup Surveillance
 * @apiDescription Return a Object containing the surveillance information.
 *
 * @apiParam {String} id Surveillances unique ID.
 *
 * @apiSuccess {Object} Surveillance Object.
 * @apiError {Object} Error Return the error
 */
apiRouter.get('/surveillance/:surveillanceId', isAuthenticated, (req, res) => {
    const surveillanceId = req.params.surveillanceId;
    console.log('router /surveillance/' + surveillanceId);
    model.Surveillance.findById(surveillanceId).exec().then((surveillance) => {
        res.json(surveillance);
    }).catch((err) => {
        console.log(err);
        res.json({error: err});
    });
});

const createImageThumbnail = (image, width, height, compression) => {
    return new Promise((resolve, reject) => {
        const destination = `/uploads/${width}x${height}-${image.filename}`;
        sharp(image.path)
            .resize(width, height)
            .jpeg({
                'quality': compression,
            })
            .toFile('public' + destination)
            .then(() => {
                resolve(destination);
            }).catch((err) => {
                reject(err);
            });
    });
};

const uploadImage = upload.single('image');

/**
 * @api {post} /rest/surveillance/add Add new surveillance
 * @apiName AddSurveillance
 * @apiGroup Surveillance
 * @apiDescription Add the surveillance item given in the request body.
 *
 * @apiSuccess {Object} Surveillance Object created.
 * @apiError {Object} Error Return the error
 */
apiRouter.post('/surveillance/add', isAuthenticated, (req, res) => {
    console.log('router /surveillance/add');

    uploadImage(req, res, (err) => {
        if (err) {
            // An error occurred when uploading
            return console.log('An error occurred when uploading');
        }

        // Everything went fine
        const surveillance = req.body.surveillance;
        const image = req.file;

        const thumbnailPromise = createImageThumbnail(image, 320, 300, 60);
        const imagePromise = createImageThumbnail(image, 768, 720, 60);

        Promise.all([thumbnailPromise, imagePromise]).then((destinations) => {
            const thumbnailUrl = destinations[0];
            const imageUrl = destinations[1];
            const originalImageUrl = image.destination + image.filename;

            const newSurveillance = {
                time: new Date(),
                category: surveillance.category,
                title: surveillance.title,
                details: surveillance.description,
                coordinates: JSON.parse(surveillance.location),
                thumbnail: thumbnailUrl,
                image: imageUrl,
                original: originalImageUrl,
            };

            model.Surveillance.create(newSurveillance).then((surveillance) => {
                const response = {
                    created: true,
                    surveillance: surveillance,
                };
                console.log(response);
                res.json(response);
            }).catch((err) => {
                const response = {
                    created: false,
                    error: 'Error creating the surveillance',
                };
                res.status(404).json(response);
            });
        }).catch((err) => {
            res.status(404).json({
                created: false,
                error: 'Error processing the image',
            });
        });
    });
});

/**
 * @api {put} /rest/surveillance/:surveillanceId/edit Modify surveillance information
 * @apiName EditSurveillance
 * @apiGroup Surveillance
 * @apiDescription Update the surveillance item with the data given in the request body.
 *
 * @apiParam {String} id Surveillances unique ID.
 *
 * @apiSuccess {Object} Surveillance Object modified.
 * @apiError {Object} Error Return the error
 */
apiRouter.put('/surveillance/:surveillanceId/edit', isAuthenticated, (req, res) => {
    const surveillanceId = req.params.surveillanceId;
    console.log('router /surveillance/' + surveillanceId + '/edit');

    uploadImage(req, res, (err) => {
        if (err) {
            // An error occurred when uploading
            return console.log('An error occurred when uploading');
        }

        // Everything went fine
        const surveillance = req.body.surveillance;
        const image = req.file;

        console.log(surveillance);
        console.log(image);

        const surveillanceData = {
            time: new Date(),
            category: surveillance.category,
            title: surveillance.title,
            details: surveillance.description,
            coordinates: JSON.parse(surveillance.location),
        };

        if(image!==undefined) {
            const thumbnailPromise = createImageThumbnail(image, 320, 300, 60);
            const imagePromise = createImageThumbnail(image, 768, 720, 60);

            Promise.all([thumbnailPromise, imagePromise]).then((destinations) => {
                const thumbnailUrl = destinations[0];
                const imageUrl = destinations[1];
                const originalImageUrl = image.destination + image.filename;

                surveillanceData.thumbnail = thumbnailUrl;
                surveillanceData.image = imageUrl;
                surveillanceData.original = originalImageUrl;

                model.Surveillance.findByIdAndUpdate(surveillanceId, {$set: surveillanceData}, {new: false}, (err, surveillance) => {
                    if (err) {
                        res.status(404).json({
                            updated: false,
                            error: 'Error updating the surveillance',
                        });
                    }

                    fs.unlinkSync('./public' + surveillance.thumbnail);
                    fs.unlinkSync('./public' + surveillance.image);
                    fs.unlinkSync(surveillance.original);

                    res.json({
                        updated: true,
                        surveillance: surveillanceData,
                    });
                });
            }).catch((err) => {
                res.status(404).json({
                    created: false,
                    error: 'Error processing the image',
                });
            });
        } else {
            model.Surveillance.findByIdAndUpdate(surveillanceId, {$set: surveillanceData}, {new: true}, (err, surveillance) => {
                if (err) {
                    res.status(404).json({
                        updated: false,
                        error: 'Error updating the surveillance',
                    });
                }
                res.json({
                    updated: true,
                    surveillance: surveillance,
                });
            });
        }
    });
});

/**
 * @api {delete} /rest/surveillance/:surveillanceId/delete Delete surveillance
 * @apiName DeleteSurveillance
 * @apiGroup Surveillance
 * @apiDescription Delete the surveillance item.
 *
 * @apiParam {String} id Surveillances unique ID.
 *
 * @apiSuccess {Object} Message Deletion confirmation message with status.
 */
apiRouter.delete('/surveillance/:surveillanceId/delete', isAuthenticated, (req, res) => {
    const surveillanceId = req.params.surveillanceId;
    console.log('router /surveillance/' + surveillanceId + '/delete');

    model.Surveillance.findById(surveillanceId, (err, surveillance) => {
        if (err) {
            res.json(err);
        }

        console.log(surveillance);

        fs.unlinkSync('./public' + surveillance.thumbnail);
        fs.unlinkSync('./public' + surveillance.image);
        fs.unlinkSync(surveillance.original);

        surveillance.remove(() => {
            res.json({
                status: 'OK',
                message: 'Surveillance #' + surveillance._id + ' removed.',
            });
        }); // Removes the document
    });
});

app.all('*', (req, res) => {
    if(res.status(404)) {
        res.status(404).json({error: 'This endpoint does not exist'});
    }
});
