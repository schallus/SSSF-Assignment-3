/* global $ jQuery google document Headers Request fetch */

'use strict';

let currentCategory = 'All';
let surveillanceObject;
let map;
let marker;

// Default map center
let mapCenter = new google.maps.LatLng({
    lat: 60.184200,
    lng: 24.934500,
});

const displayFilters = () => {
    const surveillanceCategory = $('#surveillanceCategory');
    
    surveillanceCategory.find('input').not('input[data-filter=All]').parent('label').remove();
    surveillanceCategory.find('input').not('input[data-filter=All]').remove();

    const categories = getSurveillancesCategories();
    for(const cat of categories) {
        const filterButton = $(`<label class="btn btn-secondary">
            <input type="radio" name="category" data-filter="${cat}" autocomplete="off">${cat}
        </label>`);

        filterButton.appendTo(surveillanceCategory);
    }

    surveillanceCategory.find('input[name="category"]').on('change', (event) => {
        event.preventDefault();

        // Get the clicked category
        const clickedCategory = $(event.currentTarget).data('filter');

        // If the category changed, we display the surveillance items
        if(currentCategory != clickedCategory) {
            currentCategory = clickedCategory;

            displaySurveillances(currentCategory);
        }
    });
};

const deleteSurveillanceById = (id) => {
    const url = '/rest/surveillance/' + id + '/delete';
    fetch(url, {method: 'delete'}).then((resp) => {
        return resp.json();
    }).then((json) => {
        console.log(json);
        updateSurveillanceList();
    });
}

const displaySurveillances = (category) => {

    const surveillanceList = $('#surveillanceList');

    // Delete all the surveillance items
    surveillanceList.html('');

    // Filter the element
    const surveillanceArray = getSurveillancesByCategory(category);

    if(surveillanceArray.length === 0) {
        surveillanceList.append(`<div class="alert alert-warning">
            <strong>Error!</strong> There is nothing to display in this category
        </div>`);
    } else {
        for(const surveillance of surveillanceArray) {
            const surveillanceItem = `<div class="col-lg-4 col-md-6 col-xs-12">
                <div class="card">
                    <img class="card-img-top" src="${surveillance.thumbnail}" alt="${surveillance.title}">
                    <div class="card-block">
                        <h4 class="title">${surveillance.title}</h4>
                        <div class="card-text">
                            <p class="date">${new Date(surveillance.time).toLocaleString()}</p>
                            <p class="details">${surveillance.details}</p>
                            <p>
                                <button type="button" class="btn btn-primary view-button" data-toggle="modal" data-id="${surveillance._id}">View</button>
                                <button type="button" class="btn btn-primary edit-button" data-toggle="modal" data-id="${surveillance._id}">Edit</button>
                                <button type="button" class="btn btn-primary delete-button" data-toggle="modal" data-id="${surveillance._id}">Delete</button>
                            </p>
                        </div>
                    </div>
                </div>`;

            // Add an item to the list
            surveillanceList.append(surveillanceItem);
        }

        // On view button click
        $(surveillanceList).find('.view-button').on('click', (event) => {
            // We get the clicked surveillance object
            const surveillanceId = $(event.currentTarget).data('id');
            const surveillance = getSurveillanceById(surveillanceId);

            // Add a marker on the map to the surveillance coordinates
            mapAddMarker(
                surveillance.coordinates.lat,
                surveillance.coordinates.lng,
                surveillance.title
            );

            // Create a modal and display it
            const modal = $('#surveillanceDetails');
            modal.find('.modal-title').text(surveillance.title);

            const modalBody = modal.find('.modal-body');

            const surveillanceImage = $('<img>').attr({
                src: surveillance.image,
            });
            modalBody.find('#surveillanceImage').html(surveillanceImage);

            $('#surveillanceDetails').modal({
                'backdrop': true,
                'show': true,
                'keyboard': true,
            }).on('shown.bs.modal', () => {
                google.maps.event.trigger(map, 'resize');
                map.setCenter(mapCenter);
            });
        });

        // On edit button click
        $(surveillanceList).find('.edit-button').on('click', (event) => {
            const surveillanceId = $(event.currentTarget).data('id');
            loadFormData(surveillanceId);
            $('#mainNavigation li:eq(1) a').tab('show');
        });

        // On delete button click
        $(surveillanceList).find('.delete-button').on('click', (event) => {
            const surveillanceId = $(event.currentTarget).data('id');
            deleteSurveillanceById(surveillanceId);
        });
    }
};

const emptyForm = () => {
    const form = $('#addSurveillanceForm');
    form.find('#inputId').val('');
    form.find('#inputCategory').val('');
    form.find('#inputTitle').val('');
    form.find('#inputLocation').val('');
    form.find('#textareaDescription').val('');
    form.find('#imageInput').val('');
};

const loadFormData = (surveillanceId) => {
    console.log('Load form data');
    const surveillance = getSurveillanceById(surveillanceId);
    console.log(surveillance);
    const form = $('#addSurveillanceForm');
    form.find('#inputId').val(surveillance._id);
    form.find('#inputCategory').val(surveillance.category);
    form.find('#inputTitle').val(surveillance.title);
    form.find('#inputLocation').val(JSON.stringify(surveillance.coordinates));
    form.find('#textareaDescription').val(surveillance.details);
    form.find('#imageInput').val('');
};

const getSurveillanceById = (id) => {
    return surveillanceObject.filter((surveillance) => surveillance._id == id)[0];
};

const getSurveillancesByCategory = (category) => {
    if(category == 'All') {
        return surveillanceObject;
    } else {
        return surveillanceObject.filter((surveillance) => surveillance.category == category);
    }
};

const getSurveillancesCategories = () => {
    const categories = surveillanceObject.map((surveillance) => surveillance.category)
        .filter((elem, pos, arr) => arr.indexOf(elem) === pos);
    return categories;
};

const initMap = () => {
    map = new google.maps.Map($('#surveillanceLocation').get(0), {
        center: mapCenter,
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    });
};

const mapAddMarker = (lat, lng, title) => {
    // Remove the marker if there is already one
    if(marker !== undefined) {
        marker.setMap(null);
    }

    mapCenter = new google.maps.LatLng(lat, lng);

    marker = new google.maps.Marker({
        position: mapCenter,
        map: map,
        title: title,
    });

    // When we create a marker, we set the map center to the marker location
    map.setCenter(mapCenter);
};

const fetchJSONData = (url, callback) => {
    const myHeaders = new Headers({
        'Content-Type': 'application/json',
    });

    const myInit = {
        method: 'GET',
        headers: myHeaders,
        cache: 'default',
    };

    const myRequest = new Request(url+'?username=floriasc&password=Password123', myInit);
    fetch(myRequest).then((response) => {
        if(response.ok) {
            const contentType = response.headers.get('Content-Type');
            if(contentType.indexOf('application/json') > -1) {
                return response.json();
            } else {
                throw new Error('The Content-Type is wrong.');
            }
        } else {
            throw new Error('Error loading the JSON content.');
        }
    }).then((json) => {
        callback(json);
    }).catch((e) => {
        console.log('Problem :( ' + e.message);

        $('#surveillanceList').append(`<div class="alert alert-danger">
            <strong>Error!</strong> ${e.message}
        </div>`);
    });
};

const updateSurveillanceList = () => {
    // Get data from the server
    fetchJSONData('/rest/surveillances', (json) => {
        surveillanceObject = json;

        console.log(surveillanceObject);

        // Once data loaded, we display them
        displayFilters();
        displaySurveillances(currentCategory);
    });
}

const getAddressCoordinates = (address) => {
    return new Promise((resolve, reject) => {
        let geocoding = false;
        let coordinates;

        try {
            coordinates = JSON.parse(address);
        } catch(err) {
            geocoding = true;
        }

        if(geocoding) {
            const geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status === 'OK') {
                    const coordinates = {
                        lat: results[0].geometry.location.lat(),
                        lng: results[0].geometry.location.lng(),
                    };

                    resolve(coordinates);
                } else {
                    reject('The address is incorrect');
                }
            });
        } else {
            resolve(coordinates);
        }
    });
}

jQuery(document).ready(($) => {
    // Init the Google Map
    initMap();

    updateSurveillanceList();

    // On Google Map hover, we change the size of the map
    $('#surveillanceLocation').hover((event) => {
        $(event.currentTarget).animate({
            width: $(event.currentTarget).parent().width(),
            height: $(event.currentTarget).parent().height(),
        }, 200, () => {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapCenter);
        });
    }, (event) => {
        $(event.currentTarget).animate({
            width: '100px',
            height: '100px',
        }, 200, () => {
            google.maps.event.trigger(map, 'resize');
            map.setCenter(mapCenter);
        });
    });

    const addSurveillanceForm = $('#addSurveillanceForm');

    addSurveillanceForm.submit((event) => {
        event.preventDefault();

        const formData = new FormData(event.target);

        getAddressCoordinates(formData.get('surveillance[location]'))
        .then((coordinates) => {
            console.log(coordinates);
            formData.set('surveillance[location]', JSON.stringify(coordinates));

            let method;
            let url;

            if(formData.get('surveillance[id]')!=='') {
                console.log('update');
                method = 'put';
                url = '/rest/surveillance/' + formData.get('surveillance[id]') + '/edit';
            } else {
                console.log('create');
                method = 'post';
                url = '/rest/surveillance/add';
            }

            fetch(url, {
                    method: method,
                    body: formData,
                }
            ).then((resp) => {
                return resp.json();
            }).then((json) => {
                console.log(json);
                emptyForm();
                updateSurveillanceList();
                $('#mainNavigation li:first a').tab('show');
            });
        }).catch((err) => {
            console.log(err);
        });
    });
});
